<?php


namespace App;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderCanShip extends OrderAcceptance
{
    /**
     * the common method that each child class should have
     * and perform action in it like checking the users credit
     *
     * @param Request $request
     * @param Product $product
     * @return mixed
     */
    public function handle(Request $request, Product $product)
    {
        if ($request->user()->city !== $product->city) {
            DB::rollBack();
            abort(422, 'product con not be shipped to you');
        }
        $this->next($request, $product);
    }
}


