<?php


namespace App;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserHasCredit extends OrderAcceptance
{
    /**
     * the common method that each child class should have
     * and perform action in it like checking the users credit
     *
     * @param Request $request
     * @param Product $product
     * @return mixed
     */
    public function handle(Request $request, Product $product)
    {
        if ($request->user()->credit < $product->price) {
            DB::rollBack();
            abort(422, 'you dont have enough credit');
        }

        $request->user()->decrement('credit', $product->price);

        $this->next($request, $product);
    }
}
