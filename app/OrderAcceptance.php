<?php


namespace App;


use Illuminate\Http\Request;

abstract class OrderAcceptance
{
    /**
     * the next object in the chain
     * @var $successor
     */
    protected $successor;

    /**
     * the common method that each child class should have
     * and perform action in it like checking the users credit
     *
     * @param Request $request
     * @param Product $product
     * @return mixed
     */
    abstract public function handle(Request $request, Product $product);

    /**
     * define whats the next class
     * @param OrderAcceptance $successor
     */
    public function succeedWith(OrderAcceptance $successor)
    {
        $this->successor = $successor;
    }

    /**
     * perform the next action in the chain
     * @param Request $request
     * @param Product $product
     */
    public function next(Request $request, Product $product)
    {
        if ($this->successor) {
            $this->successor->handle($request, $product);
        }
    }
}

