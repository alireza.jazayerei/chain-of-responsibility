<?php

namespace App\Http\Controllers;

use App\OrderCanShip;
use App\Product;
use App\ProductIsAvailable;
use App\UserHasCredit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function store(Request $request, Product $product)
    {
        DB::beginTransaction();
        //new up process classes
        $orderCanShip = new OrderCanShip();
        $productIsAvailable = new ProductIsAvailable();
        $userHasCredit = new UserHasCredit();

        //set each classes next object in the chain
        $orderCanShip->succeedWith($productIsAvailable);
        $productIsAvailable->succeedWith($userHasCredit);

        //begin the chain from this point
        $orderCanShip->handle($request, $product);

        //if we got here everything is ok to submit an order
        DB::commit();

        $product->orders()->attach([
            'user_id' => auth()->id(),
        ]);
    }

    public function create()
    {
        return view('CreateOrders', [
            'products' => Product::all(),
        ]);
    }
}
