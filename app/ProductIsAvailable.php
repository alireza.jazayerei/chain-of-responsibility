<?php


namespace App;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductIsAvailable extends OrderAcceptance
{

    /**
     * the common method that each child class should have
     * and perform action in it like checking the users credit
     *
     * @param Request $request
     * @param Product $product
     * @return mixed
     */
    public function handle(Request $request, Product $product)
    {
        if (!$product->count > 0) {
            DB::rollBack();
            abort(422, 'product is not available');
        }

        $product->decrement('count');

        $this->next($request, $product);
    }
}
